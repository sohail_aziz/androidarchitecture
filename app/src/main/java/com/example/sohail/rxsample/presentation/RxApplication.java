package com.example.sohail.rxsample.presentation;

import android.app.Application;

import com.example.sohail.rxsample.presentation.dagger.ApplicationComponent;
import com.example.sohail.rxsample.presentation.dagger.ApplicationModule;
import com.example.sohail.rxsample.presentation.dagger.DaggerApplicationComponent;

/**
 * Created by Sohail on 5/30/2016.
 */
public class RxApplication extends Application {

    private ApplicationComponent applicationComponent;


    @Override
    public void onCreate() {
        super.onCreate();

        initializeAppComponent();
    }

    private void initializeAppComponent() {

        this.applicationComponent = DaggerApplicationComponent.builder().
                applicationModule(new ApplicationModule())
                .build();

    }

    public ApplicationComponent getApplicationComponent() {
        return this.applicationComponent;
    }
}
