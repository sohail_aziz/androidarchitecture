package com.example.sohail.rxsample.presentation.dagger;

import com.example.sohail.rxsample.data.NuKashAPI;
import com.example.sohail.rxsample.data.Repository;
import com.example.sohail.rxsample.data.RepositoryImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Sohail on 5/30/2016.
 */
@Module
public class ApplicationModule {

    @Singleton
    @Provides
    public Repository provideRepository(RepositoryImpl repository) {
        return repository;
    }

    @Singleton
    @Provides
    public OkHttpClient provideOkHttpClient() {

        final HttpLoggingInterceptor.Level LOG_LEVEL = HttpLoggingInterceptor.Level.BODY;

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(LOG_LEVEL);

        return new OkHttpClient.Builder().addInterceptor(loggingInterceptor).build();


    }

    @Singleton
    @Provides
    public NuKashAPI provideNuKashAPI(OkHttpClient okHttpClient) {


        final String BASE_URL = "http://223.25.232.214:2286";

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();


        return retrofit.create(NuKashAPI.class);

    }
}

