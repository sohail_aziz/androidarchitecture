
package com.example.sohail.rxsample.data.response;


public class LoginResponse {

    private String UserCode;
    private String FullName;
    private String Language;
    private String EmailAdd;
    private int Status;
    private String Message;
    private String UserName;

    @Override
    public String toString() {
        return "LoginResponse{" +
                "UserCode='" + UserCode + '\'' +
                ", FullName='" + FullName + '\'' +
                ", Language='" + Language + '\'' +
                ", EmailAdd='" + EmailAdd + '\'' +
                ", Status=" + Status +
                ", Message='" + Message + '\'' +
                ", UserName='" + UserName + '\'' +
                '}';
    }

    public String getUserName() {
        return UserName;
    }

    public String getUserCode() {
        return UserCode;
    }

    public void setUserCode(String userCode) {
        UserCode = userCode;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public String getLanguage() {
        return Language;
    }

    public void setLanguage(String language) {
        Language = language;
    }

    public String getEmailAdd() {
        return EmailAdd;
    }

    public void setEmailAdd(String emailAdd) {
        EmailAdd = emailAdd;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }
}