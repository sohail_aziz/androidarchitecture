package com.example.sohail.rxsample.presentation;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.sohail.rxsample.R;
import com.example.sohail.rxsample.presentation.LoginFragment;
import com.example.sohail.rxsample.presentation.dagger.ApplicationComponent;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new LoginFragment()).commit();
        }
    }


}
