package com.example.sohail.rxsample.domain;

import com.example.sohail.rxsample.data.Repository;
import com.example.sohail.rxsample.presentation.dagger.PerActivity;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscriber;

/**
 * Created by Sohail on 5/27/2016.
 */
@PerActivity
public class LoginUsecase extends Usecase {


    private final Repository repository;
    private Observable observable;

    @Inject
    public LoginUsecase(Repository repository) {
        this.repository = repository;

    }

    public void login(String username, String password,Subscriber subscriber) {

        this.observable = this.repository.login(username, password);
        execute(subscriber);

    }



    @Override
    protected Observable buildObservable() {
        return observable;
    }


}
