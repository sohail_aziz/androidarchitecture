package com.example.sohail.rxsample.presentation;

import android.support.annotation.NonNull;

import com.example.sohail.rxsample.data.response.LoginResponse;
import com.example.sohail.rxsample.domain.LoginUsecase;

import javax.inject.Inject;

/**
 * Created by Sohail on 5/27/2016.
 */
public class LoginPresenter {

    interface LoginView {
        void onLoginSuccess();

        void onLoginFailure(String error);
    }

    private LoginView loginView;
    private final LoginUsecase loginUsecase;

    @Inject
    public LoginPresenter(LoginUsecase loginUsecase) {
        this.loginUsecase = loginUsecase;
    }

    public void setView(LoginView view) {
        this.loginView = view;
    }

    /**
     * Logins with username,password
     *
     * @param username
     * @param password
     */
    public void login(@NonNull String username, @NonNull String password) {

        loginUsecase.login(username, password, new LoginSubscriber());

    }

    private final class LoginSubscriber extends rx.Subscriber<LoginResponse> {

        @Override
        public void onCompleted() {

            //operation completed status
        }

        @Override
        public void onError(Throwable e) {

            //get the error here
            loginView.onLoginFailure(e.getMessage());

        }

        @Override
        public void onNext(LoginResponse loginResponse) {

            //we will get the result here
            loginView.onLoginSuccess();
        }
    }

    ;
}
