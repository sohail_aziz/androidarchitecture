package com.example.sohail.rxsample;

/**
 * Created by Sohail on 5/27/2016.
 */
public class StringUtils {

    public static boolean isBlank(CharSequence string) {

        return (string == null || string.toString().trim().length() == 0);
    }
}
