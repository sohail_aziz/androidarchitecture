package com.example.sohail.rxsample.data;

import android.support.annotation.NonNull;

import com.example.sohail.rxsample.data.request.LoginRequest;
import com.example.sohail.rxsample.data.response.LoginResponse;

import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Call;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;

/**
 * Created by Sohail on 5/27/2016.
 */
@Singleton
public class RepositoryImpl implements Repository {

    private final NuKashAPI nuKashAPI;

    @Inject
    public RepositoryImpl(NuKashAPI nuKashAPI) {
        this.nuKashAPI = nuKashAPI;
    }

    @Override
    public Observable<LoginResponse> login(@NonNull final String username, @NonNull final String password) {

        return Observable.create(new Observable.OnSubscribe<LoginResponse>() {
            @Override
            public void call(Subscriber<? super LoginResponse> subscriber) {

                //retrofit: call api
                LoginRequest loginRequest = new LoginRequest();
                loginRequest.setUserName(username);
                loginRequest.setPassword(password);


                try {

                    Response<LoginResponse> response = nuKashAPI.login(loginRequest).execute();

                    //TODO check the response code and return response
                    if (response.body() != null) {
                        subscriber.onNext(response.body());
                        subscriber.onCompleted();
                    }else{
                        subscriber.onError(new Exception("invalid credentials"));
                    }


                } catch (IOException e) {

                    subscriber.onError(e);
                }

            }
        });

    }
}
