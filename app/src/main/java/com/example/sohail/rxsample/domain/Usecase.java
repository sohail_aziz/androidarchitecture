package com.example.sohail.rxsample.domain;

import rx.Observable;
import rx.Scheduler;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.Subscriptions;

/**
 * Created by Sohail on 5/27/2016.
 */
public abstract class Usecase {

    private Subscription subscription = Subscriptions.empty();

    protected abstract Observable buildObservable();

    /**
     * Executes the current usecase on background thread
     *
     * @param subscriber
     */
    protected void execute(Subscriber subscriber) {

        this.subscription = this.buildObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);

    }

    /**
     * Unsubscribe the subscriber
     */
    public void unSubscribe() {
        if (!subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }


}
