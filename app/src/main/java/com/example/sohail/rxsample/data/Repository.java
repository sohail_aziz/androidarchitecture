package com.example.sohail.rxsample.data;

import android.support.annotation.NonNull;

import com.example.sohail.rxsample.data.response.LoginResponse;

import rx.Observable;

/**
 * Created by Sohail on 5/27/2016.
 */
public interface Repository {

    Observable<LoginResponse> login(@NonNull String username, @NonNull String password);
}
