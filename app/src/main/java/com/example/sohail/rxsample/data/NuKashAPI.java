package com.example.sohail.rxsample.data;


import com.example.sohail.rxsample.data.request.LoginRequest;
import com.example.sohail.rxsample.data.response.LoginResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;


/**
 * Created by Sohail on 3/22/2016.
 */
public interface NuKashAPI {

    /**
     * User login
     *
     * @param request
     * @return
     */
    @Headers("Content-Type: application/json")
    @POST("/api/consumer/auth")
    Call<LoginResponse> login(@Body LoginRequest request);
}
