package com.example.sohail.rxsample.presentation.dagger;

import com.example.sohail.rxsample.data.NuKashAPI;
import com.example.sohail.rxsample.data.Repository;
import com.example.sohail.rxsample.data.RepositoryImpl;
import com.example.sohail.rxsample.presentation.MainActivity;

import javax.inject.Singleton;

import dagger.Component;
import okhttp3.OkHttpClient;

/**
 * Created by Sohail on 5/30/2016.
 */
@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(MainActivity mainActivity);

//    //expose to sub graphs
//    OkHttpClient provideOkHttpClient();
//
    Repository provideRepository();

}
