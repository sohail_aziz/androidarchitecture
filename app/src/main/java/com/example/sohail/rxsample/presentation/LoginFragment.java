package com.example.sohail.rxsample.presentation;

import butterknife.BindView;
import butterknife.ButterKnife;


import android.app.Application;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.example.sohail.rxsample.R;
import com.example.sohail.rxsample.StringUtils;
import com.example.sohail.rxsample.data.RepositoryImpl;
import com.example.sohail.rxsample.domain.LoginUsecase;
import com.example.sohail.rxsample.presentation.dagger.ApplicationComponent;
import com.example.sohail.rxsample.presentation.dagger.DaggerUserComponent;
import com.example.sohail.rxsample.presentation.dagger.UserComponent;
import com.example.sohail.rxsample.presentation.dagger.UserModule;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment implements LoginPresenter.LoginView {


    @BindView(R.id.edit_text_username)
    EditText editTextUsername;
    @BindView(R.id.edit_text_password)
    EditText editTextPassword;

    @BindView(R.id.text_input_username)
    TextInputLayout textInputLayoutUsername;

    @BindView(R.id.text_input_password)
    TextInputLayout textInputLayoutPassword;

    @BindView(R.id.button_login)
    Button buttonLogin;

    @Inject
    LoginPresenter loginPresenter;

    private UserComponent userComponent;

    private Unbinder unbinder;

    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void injectDependencies() {

        ApplicationComponent appComponent = ((RxApplication) getActivity().getApplication()).getApplicationComponent();

        this.userComponent = DaggerUserComponent.builder()
                .applicationComponent(appComponent)
                .userModule(new UserModule())
                .build();

        this.userComponent.inject(this);


    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);



        injectDependencies();
        loginPresenter.setView(this);
    }



    @OnClick(R.id.button_login)
    public void onLogin(View view) {


        final String username = editTextUsername.getText().toString();
        final String password = editTextPassword.getText().toString();

        boolean validated = false;
        //validate username
        if (StringUtils.isBlank(username)) {
            textInputLayoutUsername.setError("Email required");
            validated = false;
        } else {
            textInputLayoutPassword.setErrorEnabled(false);
            validated = true;
        }

        //validate password
        if (StringUtils.isBlank(password)) {
            textInputLayoutPassword.setError("Password required");
            validated = false;
        } else {
            textInputLayoutPassword.setErrorEnabled(false);
            validated = true;
        }

        if (validated) {
            loginPresenter.login(username, password);
        }

    }

    @Override
    public void onLoginSuccess() {

        Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLoginFailure(String error) {
        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
    }
}
