package com.example.sohail.rxsample.presentation.dagger;

import com.example.sohail.rxsample.presentation.LoginFragment;

import dagger.Component;

/**
 * Created by Sohail on 5/30/2016.
 */
@PerActivity
@Component (dependencies = ApplicationComponent.class,modules = UserModule.class)
public interface UserComponent {

    //define injection points
    void inject(LoginFragment loginFragment);
}
