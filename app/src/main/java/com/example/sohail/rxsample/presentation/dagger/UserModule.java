package com.example.sohail.rxsample.presentation.dagger;

import com.example.sohail.rxsample.data.Repository;
import com.example.sohail.rxsample.data.RepositoryImpl;
import com.example.sohail.rxsample.domain.LoginUsecase;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Sohail on 5/30/2016.
 */
@PerActivity
@Module
public class UserModule {

    //provide activity/fragment based dependencies related to single user

    //login usecase
    @PerActivity
    @Provides
    LoginUsecase provideLoginUsecase(Repository repository) {

        return new LoginUsecase(repository);
    }
}
